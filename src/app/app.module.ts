import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {GalleryComponent} from './gallery/gallery.component';
import {GalleryService} from './gallery/gallery.service';
import {ModalComponent} from './modal/dialog/modal.component';
import {ModalShowDirective} from './modal/modal-show.directive';
import {AppRoutingModule} from './app-routing.module';
import {AdminComponent} from './admin/admin.component';
import {AdminProjectsComponent} from './admin/projects/admin-projects.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ModalHideDirective} from './modal/modal-hide.directive';
import {MessageService} from './modal/messageService';
import {StaticDialogComponent} from './modal/static/static-dialog.component';
import {ModalObserverService} from './modal/ModalObserverService';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { FitToParentDirective } from './utils/fit-to-parent.directive';
import { NgInitDirective } from './utils/ng-init.directive';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    ModalComponent,
    ModalShowDirective,
    AdminComponent,
    AdminProjectsComponent,
    ModalHideDirective,
    StaticDialogComponent,
    FitToParentDirective,
    NgInitDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    DragDropModule
  ],
  providers: [
    GalleryService,
    MessageService,
    ModalObserverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
