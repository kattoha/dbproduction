import {GalleryApplication} from '../gallery/galleryApplication';

export const applications: GalleryApplication[] = [
  {
    id: 1,
    name: 'Production plan',
    image: 'https://images.unsplash.com/photo-1417436026361-a033044d901f?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;w=1080&amp;fit=max&amp;s=faa4e192f33e0d6b7ce0e54f15140e42',
    description: 'View plan of RawData, Production DB, Inegration deliveries approved by Customer',
    link: null,
    orderIndex: 1,
    group: 1
  }, {
    id: 2,
    name: 'Raw Data Management System (RDMS)',
    image: 'https://images.unsplash.com/44/MIbCzcvxQdahamZSNQ26_12082014-IMG_3526.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=49dab7a5e4b2e28b5707bc2db974c94b',
    description: 'Manage input data for DB compilation',
    link: 'http://demdlxrdms2:3000/',
    orderIndex: 2,
    group: 1
  }, {
    id: 3,
    name: 'E.A.G.L.E.',
    image: 'https://images.unsplash.com/photo-1425668273332-3a46ab26b161?ixlib=rb-0.3.5&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;w=1080&amp;fit=max&amp;s=d453ab3dec298c43415526458b360fa6',
    description: 'Launch and manage Production DB compilations',
    link: 'http://demdlxspider2:8080/',
    orderIndex: 3,
    group: 1
  }, {
    id: 4,
    name: '4',
    image: 'https://images.unsplash.com/photo-1423145369430-a9ea0de096cd?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 4,
    group: 1
  }, {
    id: 5,
    name: '5',
    image: 'https://images.unsplash.com/photo-1432887382605-0abf9cc49e8f?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 5,
    group: 1
  }, {
    id: 6,
    name: '6',
    image: 'https://images.unsplash.com/uploads/14122811862445bc266cf/931448f2?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 6,
    group: 1
  }, {
    id: 7,
    name: '7',
    image: 'https://images.unsplash.com/photo-1417026846249-f31f7e43fc35?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 7,
    group: 1
  }, {
    id: 8,
    name: '8',
    image: 'https://images.unsplash.com/uploads/14122811862445bc266cf/931448f2?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 8,
    group: 1
  }, {
    id: 9,
    name: '9',
    image: 'https://images.unsplash.com/photo-1423145369430-a9ea0de096cd?crop=entropy&amp;fit=crop&amp;fm=jpg&amp;h=1375&amp;ixjsv=2.1.0&amp;ixlib=rb-0.3.5&amp;q=80&amp;w=1725',
    description: 'Some description',
    link: null,
    orderIndex: 9,
    group: 1
  }
];
