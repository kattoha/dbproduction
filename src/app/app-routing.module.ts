import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GalleryComponent} from './gallery/gallery.component';
import {AdminComponent} from './admin/admin.component';
import {AdminProjectsComponent} from './admin/projects/admin-projects.component';

const routes: Routes = [
  {
    path: 'gallery',
    component: GalleryComponent,
    pathMatch: 'full'
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'projects',
        component: AdminProjectsComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'gallery'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
