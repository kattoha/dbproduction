import {Component, OnInit} from '@angular/core';
import {GalleryService} from '../../gallery/gallery.service';
import {GalleryApplication} from '../../gallery/galleryApplication';
import {MessageService} from '../../modal/messageService';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {ApplicationGroup} from '../../gallery/ApplicationGroup';

@Component({
  selector: 'app-projects',
  templateUrl: './admin-projects.component.html',
  styleUrls: ['./admin-projects.component.css', '../../modal/dialog/modal.component.css']
})
export class AdminProjectsComponent implements OnInit {
  public groups: ApplicationGroup[] = [];
  public selectedGroup: ApplicationGroup = new ApplicationGroup(null, null, null, []);
  public selectedApplication: GalleryApplication = new GalleryApplication(null, null, null, null, null, null);
  public activeTab = -1;

  constructor(private galleryService: GalleryService, private messageService: MessageService) {
  }

  ngOnInit(): void {

    this.loadGroups().then(() => {
      this.activeTab = this.groups.length - 1;
      this.setEditGroup(null);
      this.setEditApplication(null);
    }).catch(reason => this.messageService.error('Cannot load groups: ' + reason.message));
  }

  public removeApplication(index: number): void {
    this.messageService.confirm('Delete this application?').then(() => {
      const group = this.groups[this.activeTab];
      const application = group.applications[index];
      if (application.id == null) {
        group.applications.splice(index, 1);
      } else {
        this.galleryService.removeApplication(application.id).subscribe(() => {
          this.loadGroups().then();
        }, error => this.messageService.error(error.message));
      }
    }).catch(reason => console.log(reason));
  }

  /**
   * Push gallery application to edit mode
   * @param application GalleryProject (nullable). If parameter is null will set new empty application
   */
  public setEditApplication(application: GalleryApplication): void {
    if (application == null) {
      const group = this.groups[this.activeTab];
      this.selectedApplication = new GalleryApplication(null, null, null, null, null, group.id);
    } else {
      this.selectedApplication = JSON.parse(JSON.stringify(application));
    }
  }

  /**
   * Push application group to edit mode
   * @param group ApplicationGroup (nullable). If parameter is null will set new empty group
   */
  public setEditGroup(group: ApplicationGroup): void {
    if (group == null) {
      this.selectedGroup = new ApplicationGroup(null, null, null, []);
    } else {
      this.selectedGroup = JSON.parse(JSON.stringify(group));
    }
  }

  /**
   * Save selected application
   */
  public saveSelectedApplication(): void {
    if (this.selectedApplication != null) {
      this.galleryService.saveApplication(this.selectedApplication).subscribe(() => {
        this.messageService.info('Application saved').then();
        this.loadGroups().then();
      }, error => this.messageService.error(error));
    }
  }

  /**
   * Set new image to currently selected project
   * @param event target event
   */
  public setImage(event): void {
    const maxWidth = 300;
    const file = event.target.files[0];
    const reader: FileReader = new FileReader();
    console.log(file);
    reader.onload = ev => {
      const src = ev.target.result;
      this.compressImage(src, maxWidth).then(value => {
        console.log(value);
        this.selectedApplication.image = value;
      }).catch(reason => console.error(reason));
    };
    reader.readAsDataURL(file);
  }

  /**
   * Handle DnD event while order index changing
   * @param event CdkDragDrop event
   */
  public changeApplicationOrder(event: CdkDragDrop<GalleryApplication>): void {
    const group = this.groups[this.activeTab];
    if (group.applications.length > 1 && event.item.element.nativeElement.classList.contains("application-row")) {
      const currentIndex = event.currentIndex;
      const previousIndex = event.previousIndex;
      const application1 = group.applications[previousIndex];
      const application2 = group.applications[currentIndex];
      this.galleryService.swapApplicationsOrder(application1, application2).subscribe(() => {
        this.loadGroups().then();
      }, error => {
        this.messageService.error('Cannot change application order index: ' + error.message, 'Error is occurred').then();
      });
    }
  }

  public saveSelectedGroup(): void {
    const group: ApplicationGroup = this.selectedGroup;
    const isNewGroup: boolean = group.id == null;
    this.galleryService.saveGroup(group).subscribe(response => {
      if (isNewGroup) {
        this.groups.push(response);
        this.activeTab++;
      } else {
        this.groups[this.activeTab] = response;
      }
      this.messageService.info('Group has been saved successfully', 'Success').then();
    }, error => {
      this.messageService.error(error.message).then();
    });
  }

  public removeSelectedGroup(moveAppsTo: number = 0): void {
    const groupID: number = this.selectedGroup.id;
    if (groupID == null) {
      return;
    }
    this.galleryService.removeGroup(groupID, moveAppsTo).subscribe(() => {
      this.groups.pop();
      this.activeTab--;
      this.loadGroups().then(() => {
      });
    });
  }

  private loadGroups(): Promise<void> {
    return this.galleryService.loadGroups().forEach(value => {
      this.groups = value;
      return value;
    });
  }

  private compressImage(src, maxWidth): Promise<string> {
    return new Promise((res, rej) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        const scaleFactor = maxWidth / img.width;
        const elem = document.createElement('canvas');
        elem.width = maxWidth;
        elem.height = img.height * scaleFactor;
        const ctx = elem.getContext('2d');
        ctx.drawImage(img, 0, 0, maxWidth, img.height * scaleFactor);
        const data = ctx.canvas.toDataURL('image/jpeg', 1);
        res(data);
      };
      img.onerror = error => rej(error);
    });
  }

}
