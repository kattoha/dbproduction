import {Directive, HostListener, Input, OnInit} from '@angular/core';
import {ModalObserverService} from './ModalObserverService';
import {IModal} from './IModal';

@Directive({
  selector: '[appModalHide]'
})
export class ModalHideDirective implements OnInit{
  @Input("appModalHide")
  id: string;

  constructor(private modalObserverService: ModalObserverService) {
  }

  ngOnInit(): void {
    if (this.id == null) {
      console.error('Directive \'app-modal-hide\' must have value (modal ID)');
    }
  }

  @HostListener('click')
  onClick(): void {
    const modal = this.modalObserverService.getModal(this.id);
    if (!modal) {
      console.error(`Unable to find modal dialog with id: '${this.id}'`);
    } else {
      // hide modal window
      modal.hide();
    }
  }

}
