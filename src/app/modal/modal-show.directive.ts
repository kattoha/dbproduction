import {Directive, HostListener, Input, OnInit} from '@angular/core';
import {ModalObserverService} from './ModalObserverService';

/**
 * Directive which allows to open modal dialog (app-modal component)
 */
@Directive({
  selector: '[appModalShow]'
})
export class ModalShowDirective implements OnInit {
  @Input('appModalShow')
  id: string;

  constructor(private modalObserverService: ModalObserverService) {
  }

  ngOnInit(): void {
    if (this.id == null) {
      console.error('Directive \'app-modal-show\' must have value (modal ID)');
    }
  }

  @HostListener('click')
  onClick(): void {
    const modal = this.modalObserverService.getModal(this.id);
    if (!modal) {
      console.error(`Unable to find modal dialog with id: '${this.id}'`);
    } else {
      modal.show();
    }
  }

}
