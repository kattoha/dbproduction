/**
 * Common interface for modal component
 */
export interface IModal {
  getId(): string;

  show(): void;

  hide(): void;

  isVisible(): boolean;
}
