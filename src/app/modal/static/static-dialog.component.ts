import {Component, OnInit} from '@angular/core';
import {DialogState, Message, MessageService, MessageType} from '../messageService';

@Component({
  selector: 'app-static-dialog',
  templateUrl: './static-dialog.component.html',
  styleUrls: ['./static-dialog.component.css']
})
export class StaticDialogComponent implements OnInit {
  public modalId: string;
  public message: Message;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.modalId = this.messageService.modalId;
  }

  show(): void {
    this.message = this.messageService.message;
  }

  hide(dialogState: string): void {
    let state = DialogState.UNDEFINED;
    switch (dialogState) {
      case 'reject':
      case 'cancel':
        state = DialogState.REJECTED;
        break;
      case 'accept':
      case 'ok':
        state = DialogState.ACCEPTED;
        break;
    }
    this.messageService.hide(state);
  }
}
