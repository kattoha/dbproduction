import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticDialogComponent } from './static-dialog.component';

describe('StaticDialogComponent', () => {
  let component: StaticDialogComponent;
  let fixture: ComponentFixture<StaticDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaticDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
