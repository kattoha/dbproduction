import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IModal} from '../IModal';
import {ModalObserverService} from '../ModalObserverService';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements IModal, OnInit, OnDestroy {
  // Input data
  @Input()
  public id: string;
  @Input()
  public width: string;
  @Input()
  public onShow: () => any;
  @Input()
  public onHide: () => any;
  /**
   * If true element won't be appended to the end of the body tag
   */
  @Input()
  public doNotMove: boolean;
  @Input()
  public title;
  // Variables
  private element: HTMLElement;
  protected dialog: HTMLElement;
  private visibilityObserver: MutationObserver;

  constructor(private modalObserverService: ModalObserverService) {
  }

  getId(): string {
    return this.id;
  }

  show(): void {
    this.dialog.classList.add('visible');
  }

  hide(): void {
    this.dialog.classList.remove('visible');
  }

  isVisible(): boolean {
    return this.dialog.classList.contains('visible');
  }

  ngOnInit(): void {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }
    this.modalObserverService.register(this);
    this.element = document.querySelector(`app-modal[ng-reflect-id='${this.id}']`);
    this.dialog = this.element.firstChild as HTMLElement;
    this.visibilityEventInterceptor();
    // append block to body
    if (!this.doNotMove) {
      document.getElementsByTagName('body')[0].appendChild(this.element);
    }
    // Handle closing via clicking outside the modal body
    window.addEventListener('mouseup', e => {
      if (!this.isDescendant(this.element.querySelector('.ng-modal-content'), e.target)) {
        this.hide();
      }
    });
  }

  ngOnDestroy(): void {
    this.hide();
    this.modalObserverService.dispose(this);
    this.visibilityObserver.disconnect();
    this.element.remove();
  }

  private visibilityEventInterceptor(): void {
    this.visibilityObserver = new MutationObserver(((mutations: MutationRecord[]) => {
      for (const mutation of mutations) {
        if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
          const previousVisible = mutation.oldValue.indexOf('visible') >= 0;
          const currentlyVisible = (mutation.target as HTMLElement).attributes[mutation.attributeName].value.indexOf('visible') >= 0;
          if (currentlyVisible !== previousVisible) {
            if (currentlyVisible) {
              console.log('Show modal: ' + this.id);
              if (this.onShow) {
                this.onShow();
              }
            } else {
              console.log('Hide modal: ' + this.id);
              if (this.onHide) {
                this.onHide();
              }
            }
          }
        }
      }
    }));
    this.visibilityObserver.observe(this.element.querySelector('div.ng-modal'), {
      attributes: true,
      attributeFilter: ['class'],
      attributeOldValue: true
    });
  }

  /**
   * Check if child is child of parent or node == parent
   * @param parent parent node
   * @param child current node
   */
  private isDescendant(parent, child): boolean {
    let node = child;
    while (node != null) {
      if (node === parent) {
        return true;
      }
      node = node.parentNode;
    }
    return false;
  }
}
