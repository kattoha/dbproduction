import {Injectable} from '@angular/core';
import {IModal} from './IModal';

@Injectable({providedIn: 'root'})
export class ModalObserverService {
  private registered: Set<IModal> = new Set<IModal>();

  public register(modal: IModal): void {
    this.registered.add(modal);
  }

  public dispose(modal: IModal): void {
    this.registered.delete(modal);
  }

  public getModal(modalId: string): IModal {
    return [...this.registered].find(value => value.getId() === modalId);
  }
}
