import {Injectable, OnDestroy} from '@angular/core';
import {ModalObserverService} from './ModalObserverService';
import {IModal} from './IModal';

@Injectable({providedIn: 'root'})
export class MessageService implements OnDestroy {
  modalId: string;
  message: Message;
  private dialogState: DialogState = DialogState.UNDEFINED;
  private interval;

  constructor(private modalObserverService: ModalObserverService) {
    this.modalId = Date.now().toString(36) + Math.random().toString(36).substring(2);
  }

  ngOnDestroy(): void {
    this.hide(DialogState.UNDEFINED);
    this.clearScheduler();
  }

  public hide(dialogState: DialogState): void {
    this.dialogState = dialogState;
    const modal = this.modalObserverService.getModal(this.modalId);
    modal.hide();
  }

  public showMessage(message: Message): Promise<void> {
    this.message = message;
    this.dialogState = DialogState.UNDEFINED;
    const modal: IModal = this.modalObserverService.getModal(this.modalId);
    modal.show();
    return new Promise<void>((resolve, reject) => {
      this.interval = setInterval(() => {
        switch (this.dialogState) {
          case DialogState.ACCEPTED:
            resolve();
            break;
          case DialogState.REJECTED:
            reject();
            break;
          default:
          // Nothing to do;
        }
        if (!modal.isVisible()) {
          this.clearScheduler();
        }
      }, 500);
    });
  }

  public info(message: string, title: string = ''): Promise<void> {
    const msg = new Message(message, title, MessageType.INFO);
    return this.showMessage(msg);
  }

  public confirm(message: string, title: string = ''): Promise<void> {
    const msg = new Message(message, title, MessageType.CONFIRM);
    return this.showMessage(msg);
  }

  public error(message: string, title: string = ''): Promise<void> {
    const msg = new Message(message, title, MessageType.ERROR);
    return this.showMessage(msg);
  }

  public warning(message: string, title: string = ''): Promise<void> {
    const msg = new Message(message, title, MessageType.WARNING);
    return this.showMessage(msg);
  }

  private clearScheduler(): void {
    clearInterval(this.interval);
  }
}

export class Message {
  constructor(public message: string,
              public title: string,
              public type: MessageType) {
  }
}

export enum MessageType {
  INFO,
  ERROR,
  WARNING,
  SUCCESS,
  CONFIRM
}

export enum DialogState {
  UNDEFINED,
  ACCEPTED,
  REJECTED
}
