import {GalleryApplication} from './galleryApplication';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {applications as apps} from '../fixtures/applications.fixture';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ApplicationGroup} from './ApplicationGroup';

@Injectable({providedIn: 'root'})
export class GalleryService {
  constructor(private http: HttpClient) {
  }

  loadApplications(): Observable<GalleryApplication[]> {
    if (environment.production) {
      return this.http.get<GalleryApplication[]>('/rest/applications/list', {
        headers: new HttpHeaders({Accept: 'application/json'})
      }).pipe(map(value => value as GalleryApplication[]));
    } else {
      return new Observable<GalleryApplication[]>(observer => {
        setTimeout(() => {
          observer.next(apps);
          observer.complete();
        }, 200);
      });
    }
  }

  loadGroups(): Observable<ApplicationGroup[]> {
    if (environment.production) {
      return this.http.get('/rest/applications/groups', {
        headers: new HttpHeaders({Accept: 'application/json'})
      })
        .pipe(map(value => value as ApplicationGroup[]));
    } else {
      return new Observable<ApplicationGroup[]>(observer => {
        setTimeout(() => {
          observer.next([new ApplicationGroup(1, 'test group', null, apps)]);
          observer.complete();
        }, 200);
      });
    }
  }

  saveApplication(application: GalleryApplication): Observable<object> {
    // save project
    if (environment.production) {
      return this.http.post('/rest/applications/application/save', JSON.stringify(application), {
        headers: new HttpHeaders({'Content-type': 'application/json'})
      });
    } else {
      // For development
      return new Observable<GalleryApplication[]>(observer => {
        setTimeout(() => {
          if (application.id == null) {
            const maxId = Math.max(...apps.map(value => value.id)) || 0;
            application.id = maxId + 1;
            apps.push(application);
          } else {
            const indexOfExisted: number = apps.findIndex(value => value.id === application.id);
            if (indexOfExisted >= 0) {
              apps[indexOfExisted] = application;
            }
          }
          observer.next(null);
          observer.complete();
        }, 200);
      });
    }
  }

  updateOnlineStatus(application: GalleryApplication): void {
    const urlMatchRegExp = new RegExp('^https?://.+$', 'g');
    if (application.link?.match(urlMatchRegExp)) {
      const params = new URLSearchParams();
      params.append('url', application.link);
      this.http.post('/rest/applications/application/onlineStatus', params.toString(), {
        headers: new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
      }).toPromise().then(value => {
        /* tslint:disable:no-string-literal */
        application.isOnline = value['online'];
        /* tslint:enable:no-string-literal */
      }).catch(reason => {
        application.isOnline = false;
      });
    }
  }

  swapApplicationsOrder(application1: GalleryApplication, application2: GalleryApplication): Observable<GalleryApplication> {
    return this.http.get(`/rest/applications/swapOrder?application1=${application1.id}&application2=${application2.id}`).pipe(
      map(value => value as GalleryApplication)
    );
  }

  removeApplication(appID: number): Observable<object> {
    return this.http.get(`/rest/applications/application/remove/${appID}`);
  }

  /**
   * Save group data. BUT NOT included applications
   * @param group ApplicationGroup
   */
  saveGroup(group: ApplicationGroup): Observable<ApplicationGroup> {
    return this.http.post('/rest/applications/group/save', group, {
      headers: new HttpHeaders({'Content-type': 'application/json'})
    }).pipe(map(value => value as ApplicationGroup));
  }

  /**
   * Remove group
   * @param groupID removable group ID
   * @param moveAppsTo group ID where to move apps from removable group. If 0 apps will be removed too.
   */
  removeGroup(groupID: number, moveAppsTo: number = 0): Observable<any> {
    return this.http.get(`/rest/applications/group/remove/${groupID}?moveAppsTo=${moveAppsTo}`);
  }
}
