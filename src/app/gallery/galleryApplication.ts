/**
 * Model describes some DBPROD application
 */
export class GalleryApplication {


  public isOnline?: boolean;

  constructor(public id: number,
              public name: string,
              public description: string,
              public link: string,
              public image: string,
              public group: number,
              public orderIndex: number = 0) {
    this.isOnline = false;
  }
}
