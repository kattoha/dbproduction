import {GalleryApplication} from './galleryApplication';

export class ApplicationGroup {
  constructor(public id: number,
              public name: string,
              public description: string,
              public applications: GalleryApplication[],
              public hideName: boolean = false) {
  }
}
