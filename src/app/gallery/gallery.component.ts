import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {GalleryService} from './gallery.service';
import {ApplicationGroup} from './ApplicationGroup';
import {GalleryApplication} from './galleryApplication';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit, OnDestroy {

  constructor(private galleryService: GalleryService, private elementRef: ElementRef) {
    this.schedulers = [];
  }

  public groups: ApplicationGroup[] = [];
  private schedulers: number[];
  private galleryFrame: HTMLElement;

  private static between(num: number, a: number, b: number): boolean {
    const min = Math.min(a, b);
    const max = Math.max(a, b);
    return num > min && num < max;
  }

  private static percentage(a: number, b: number): number {
    const max = Math.max(a, b);
    const min = Math.min(a, b);
    const result = ((max - min) / max) * 100;
    if (result > 100) {
      return 100;
    }
    if (result < 0) {
      return 0;
    }
    return result;
  }

  private static getVisibilityPercentage(el: HTMLElement, frame: HTMLElement): number {
    const elRect: DOMRect = el.getBoundingClientRect();
    const frameRect: DOMRect = frame.getBoundingClientRect();
    const elHeight: number = elRect.height;

    const visibleTop = GalleryComponent.between(elRect.top, frameRect.top, frameRect.bottom);
    const visibleBottom = GalleryComponent.between(elRect.bottom, frameRect.top, frameRect.bottom);

    // If element fully visible
    if (visibleTop && visibleBottom) {
      return 100;
    }
    // If element visible from top
    if (visibleTop) {
      return 100 - GalleryComponent.percentage(elHeight, frameRect.bottom - elRect.top);
    }
    // If element visible from bottom
    if (visibleBottom) {
      return 100 - GalleryComponent.percentage(elHeight, elRect.bottom - frameRect.top);
    }
    return 0;
  }

  /**
   * Set opacity of element
   * @param element HTMLElement
   * @param visibilityScale scale in percentage between 0 and 100
   */
  private static setOpacity(element: HTMLElement, visibilityScale: number): void {
    if (visibilityScale > 0) {
      const scale = visibilityScale / 100;
      element.style.opacity = String((scale > 0.8 ? scale : scale * scale / 4));
      element.style.setProperty('filter', `blur(${10 - (scale * 10)}px)`);
    } else {
      element.style.opacity = '0';
      element.style.removeProperty('filter');
    }
  }

  ngOnInit(): void {

    // load from DB
    this.galleryService.loadGroups().subscribe(value => {
      this.groups = value;
      this.initGalleryFade();
      this.initSchedulers();
    });
  }

  ngOnDestroy(): void {
    while (this.schedulers.length > 0) {
      const scheduler = this.schedulers.shift();
      clearInterval(scheduler);
    }
  }

  private initGalleryFade(): void {
    this.galleryFrame = this.elementRef.nativeElement.children[0];
    const frameTopBound = this.galleryFrame.getBoundingClientRect().top;
    // this.galleryFrame.style.setProperty("max-height", `${(window.screen.height - frameTopBound) - 50}px`);
    const scroller = this.galleryFrame.children[0];

    // Add change opacity to scroll event
    scroller.addEventListener('scroll', evt => {
      const currentTarget = evt.currentTarget as HTMLElement;
      currentTarget.querySelectorAll('.hex').forEach((el: HTMLElement) => {
        const subElement: HTMLElement = el.querySelector('.hexIn');
        const visibilityScale = GalleryComponent.getVisibilityPercentage(subElement, this.galleryFrame);
        GalleryComponent.setOpacity(el, visibilityScale);
      });
    });
    // Trigger empty scroll on startup
    setTimeout(() => {
      scroller.dispatchEvent(new CustomEvent('scroll'));
    }, 200);
  }

  private initSchedulers(): void {
    this.groups.flatMap(g => g.applications).forEach((application: GalleryApplication) => {
      const scheduler = setInterval(() => {
        this.galleryService.updateOnlineStatus(application);
      }, Math.floor(Math.random() * 5000) + 5000);
      this.schedulers.push(scheduler);
    });
  }
}
