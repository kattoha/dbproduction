import {Directive, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appNgInit]',
  exportAs: 'appNgInit'
})
export class NgInitDirective implements OnInit {
  @Input() values: any = {};

  @Input() appNgInit;

  ngOnInit(): void {
    if (this.appNgInit) {
      this.appNgInit();
    }
  }

}
