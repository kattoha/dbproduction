import {Directive, ElementRef, HostListener, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appFitToParent]'
})
export class FitToParentDirective implements OnInit {
  @Input('appFitToParent')
  private selector: string;
  private parent: HTMLElement;
  private element: HTMLElement;

  constructor(private renderer: Renderer2, elementRef: ElementRef) {
    this.element = elementRef.nativeElement;
  }

  ngOnInit(): void {
    this.parent = this.element.closest(this.selector) || this.element.parentElement;
    window.dispatchEvent(new CustomEvent('resize'));
  }

  @HostListener('window:resize')
  onResize(): void {
    this.element.style.setProperty('max-height', `${this.parent.offsetHeight}px`);
  }

}
